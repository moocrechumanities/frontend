import * as types from './constants';

export const searchAction = (term, cb) => ({
  type: types.SEARCH,
  payload: term,
  cb,
});
export const searchSuccessAction = values => ({
  type: types.SEARCH_SUCCESS,
  payload: values,
});
export const setLearnerStyle = style => ({
  type: types.LEARNER_STYLE,
  payload: style,
});
export const getRecommendedCourse = () => ({
  type: types.GET_RECOMMENDED_COURSE,
});
export const getRecommendedCourseSuccess = courses => ({
  type: types.GET_RECOMMENDED_COURSE_SUCCESS,
  payload: courses,
});
