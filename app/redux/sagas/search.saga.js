import { all, fork, takeLatest, call, put, select } from 'redux-saga/effects';
import Axios from 'axios';
import firebase from 'firebase/app';
import firestore from 'redux-firestore';
import * as types from '../constants';
import { searchSuccessAction, getRecommendedCourseSuccess } from '../actions';
import 'firebase/firestore';
import 'firebase/database';

function* search({ payload, cb }) {
  const term = payload;

  const params = new URLSearchParams();

  const learnerStyle = yield select(s => s.user.learnerStyle);

  params.append('data', term);
  params.append('learnerStyle', learnerStyle || null);
  params.append('accent', null);

  if (!term) {
    if (cb) cb();
    return yield put(searchSuccessAction([]));
  }

  try {
    const response = yield call(Axios, {
      method: 'GET',
      headers: {
        'API-KEY': 'asdfasdferhbSDFWewfgfdsGERgreVeRGesdvSErthg343',
      },
      url:
        'https://mooc-connector-mapping.herokuapp.com/mooc-humanities-connector-mapping/search',
      params,
    });

    const results = response.data.data;

    const groupedCourses = results.reduce((pv, cv) => {
      const data = (pv[cv.courseTitle] && pv[cv.courseTitle].data) || [];
      pv[cv.courseTitle] = {
        courseTitle: cv.courseTitle,
        matchedIndex: cv.matchedIndex,
        pdfWeight: cv.pdfWeight,
        quizWeight: cv.quizWeight,
        data: [...data, cv.data],
      };

      return pv;
    }, {});

    console.log(groupedCourses);
    const groupedArray = Object.values(groupedCourses);
    groupedArray.sort((a, b) => a.matchedIndex - b.matchedIndex);
    const noResults = !groupedArray.length;
    yield put(searchSuccessAction(groupedArray));

    if (cb) {
      cb(noResults);
    }
  } catch (error) {
    console.log(error);
  }
}

function* getRecoCourses({ payload }) {
  // const payloadlearnerStyle = yield select(s => s.user.learnerStyle);

  if (!payload) return;

  try {
    const params = new URLSearchParams();
    params.append('learnerStyle', payload);
    const response = yield call(Axios, {
      method: 'get',
      headers: {
        'API-KEY': 'asdfasdferhbSDFWewfgfdsGERgreVeRGesdvSErthg343',
      },
      url:
        'https://mooc-connector-mapping.herokuapp.com/mooc-humanities-connector-mapping/course',
      params,
    });

    yield put(getRecommendedCourseSuccess(response.data));
  } catch (error) {}
}

function* saveLearnerStyleToDBSaga({ payload }) {
  // try {
  //   const user = yield select(s => s.firebase.profile);
  //   if(user && user.email) {
  //     console.log('add', firebase.firestore)
  //     yield firebase.firestore.add({collection:'learner_styles'}, {style: payload, user: user.email})
  //   }
  // } catch (error) {
  //   console.log(error);
  // }
}

function* saveLearnerStyleWatcher() {
  yield takeLatest(types.LEARNER_STYLE, saveLearnerStyleToDBSaga);
}

function* searchWatcher() {
  yield takeLatest(types.SEARCH, search);
}

function* recoCoursesWatcher() {
  yield takeLatest(types.LEARNER_STYLE, getRecoCourses);
}

export default function* rootSaga() {
  yield all([
    fork(searchWatcher),
    fork(recoCoursesWatcher),
    fork(saveLearnerStyleWatcher),
  ]);
}
