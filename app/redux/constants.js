export const SEARCH = 'SEARCH';
export const SEARCH_SUCCESS = 'SEARCH_SUCCESS';
export const LEARNER_STYLE = 'LEARNER_STYLE';
export const GET_RECOMMENDED_COURSE = '@@/get recommended course';
export const GET_RECOMMENDED_COURSE_SUCCESS =
  '@@/get recommended course success';
