import * as types from '../constants';

const initialState = {
  name: 'Anonymous',
  learnerStyle: '',
};

export default function(state = initialState, action) {
  switch (action.type) {
    case types.LEARNER_STYLE: {
      return {
        ...state,
        learnerStyle: action.payload,
      };
    }
    default: {
      return state;
    }
  }
}
