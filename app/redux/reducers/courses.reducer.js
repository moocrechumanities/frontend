import * as types from '../constants';

const initialState = {
  recommended: [],
  recentlySearched: [],
  searchResults: [],
  // recentlySearched: [],
};

export default function(state = initialState, { type, payload }) {
  switch (type) {
    case types.SEARCH_SUCCESS: {
      return {
        ...state,
        searchResults: payload,
      };
    }

    case types.SEARCH: {
      return {
        ...state,
        searchResults: [],
      };
    }

    case types.GET_RECOMMENDED_COURSE_SUCCESS: {
      return {
        ...state,
        recommended: payload,
      };
    }

    default: {
      return state;
    }
  }
}
