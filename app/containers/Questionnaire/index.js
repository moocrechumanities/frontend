/* eslint-disable no-plusplus */
/* eslint-disable no-restricted-syntax */
/* eslint-disable react/prop-types */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import { Formik, Field, Form } from 'formik';
import { connect } from 'react-redux';
import { withFirebase, withFirestore } from 'react-redux-firebase';
import { setLearnerStyle } from '../../redux/actions';

class CheckBoxGroup extends Component {
  render() {
    const { qIndex } = this.props;
    const { question } = this.props.question;
    return (
      <div className="m-2">
        <div className="text font-bold">{question}</div>
        {this.props.question.values.map((answer, index) => (
          <div key={answer}>
            <Field name={`${qIndex}_${index}`} type="checkbox" label={answer} />
            <span className="ml-2">{answer}</span>
          </div>
        ))}
      </div>
    );
  }
}

export class Questionnaire extends Component {
  state = {
    questiones: [
      {
        question: 'When I am learning I:',
        values: [
          'read books, articles and handouts.',
          'use examples and applications.',
          'see patterns in things.',
          'like to talk things through.',
        ],
      },
      {
        question:
          'I want to learn how to play a new board game or card game. I would:',
        values: [
          'watch others play the game before joining in.',
          'listen to somebody explaining it and ask questions.',
          'use the diagrams that explain the various stages, moves and strategies in the game.',
          'read the instructions.',
        ],
      },
      {
        question: 'When learning from the Internet I like:',
        values: [
          'videos showing how to do or make things.',
          ' audio channels where I can listen to podcasts or interviews.',
          'interesting written descriptions, lists and explanations.',
          'interesting design and visual features.',
        ],
      },
      {
        question:
          'A website has a video showing how to make a special graph or chart. There is a person speaking, some lists and words describing what to do and some diagrams. I would learn most from:',
        values: [
          'listening.',
          'seeing the diagrams.',
          'watching the actions.',
          'reading the words.',
        ],
      },
      {
        question:
          'I want to find out more about a tour that I am going on. I would:',
        values: [
          'use a map and see where the places are.',
          'read about the tour on the itinerary.',
          'look at details about the highlights and activities on the tour.',
          'talk with the person who planned the tour or others who are going on the tour.',
        ],
      },
      {
        question: 'I want to learn how to take better photos. I would:',
        values: [
          'use the written instructions about what to do.',
          'use examples of good and poor photos showing how to improve them.',
          'ask questions and talk about the camera and its features.',
          'use diagrams showing the camera and what each part does.',
        ],
      },
      {
        question:
          'When choosing a career or area of study, these are important for me:',
        values: [
          'Using words well in written communications.',
          'Working with designs, maps or charts.',
          'Communicating with others through discussion.',
          'Applying my knowledge in real situations.',
        ],
      },
      {
        question: 'I want to learn to do something new on a computer. I would:',
        values: [
          'read the written instructions that came with the program.',
          'talk with people who know about the program.',
          'follow the diagrams in a book.',
          'start using it and learn by trial and error.',
        ],
      },
      {
        question:
          ' I need to find the way to a shop that a friend has recommended. I would:',
        values: [
          'ask my friend to tell me the directions.',
          'write down the street directions I need to remember.',
          'find out where the shop is in relation to somewhere I know.',
          'use a map.',
        ],
      },
      {
        question: 'I want to learn about a new project. I would ask for:',
        values: [
          'an opportunity to discuss the project.',
          'diagrams to show the project stages with charts of benefits and costs.',
          'examples where the project has been used successfully.',
          'a written report describing the main features of the project.',
        ],
      },
      {
        question:
          'I want to assemble a wooden table that came in parts (kitset). I would learn best from:',
        values: [
          'diagrams showing each stage of the assemby.',
          'advice from someone who has done it before.',
          'watching a video of a person assembling a similar table.',
          'written instructions that came with the parts for the table.',
        ],
      },
      {
        question:
          'I have a problem with my heart. I would prefer that the doctor:',
        values: [
          'gave me something to read to explain what was wrong.',
          'showed me a diagram of what was wrong.',
          'described what was wrong.',
          'used a plastic model to show me what was wrong.',
        ],
      },
      {
        question:
          'I want to find out about a house or an apartment. Before visiting it I would want:',
        values: [
          'a discussion with the owner.',
          'a printed description of the rooms and features.',
          'to view a video of the property.',
          'a plan showing the rooms and a map of the area.',
        ],
      },
      {
        question:
          'I want to save more money and to decide between a range of options. I would:',
        values: [
          'consider examples of each option using my financial information.',
          'use graphs showing different options for different time periods.',
          'read a print brochure that describes the options in detail..',
          'talk with an expert about the options.',
        ],
      },
      {
        question: 'I prefer a presenter or a teacher who uses:',
        values: [
          'handouts, books, or readings.',
          'diagrams, charts, maps or graphs.',
          'demonstrations, models or practical sessions.',
          'question and answer, talk, group discussion, or guest speakers.',
        ],
      },
      {
        question:
          'I have finished a competition or test and I would like some feedback. I would like to have feedback:',
        values: [
          'using a written description of my results.',
          'from somebody who talks it through with me.',
          'using examples from what I have done.',
          'using graphs showing what I achieved.',
        ],
      },
    ],
    answers: [
      ['K', 'A', 'R', 'V'],
      ['V', 'A', 'R', 'K'],
      ['K', 'V', 'R', 'A'],
      ['K', 'A', 'V', 'R'],
      ['A', 'V', 'K', 'R'],
      ['K', 'R', 'V', 'A'],
      ['K', 'A', 'V', 'R'],
      ['R', 'K', 'A', 'V'],
      ['R', 'A', 'K', 'V'],
      ['K', 'V', 'R', 'A'],
      ['V', 'R', 'A', 'K'],
      ['A', 'R', 'V', 'K'],
      ['K', 'A', 'R', 'V'],
      ['K', 'R', 'A', 'V'],
      ['K', 'A', 'R', 'V'],
      ['V', 'A', 'R', 'K'],
    ],
  };

  getLearnerStyle = values => {
    let style = '';
    let maxValue = 0;
    for (const key in values) {
      if (values[key] > maxValue) {
        style = key;
        maxValue = values[key];
      } else if (values[key] === maxValue && maxValue > 0) {
        style += key;
      }
    }

    return style;
  };

  render() {
    return (
      <div className="container h-full mx-auto mb-4 m-4 p-4 rounded shadow bg-gray-100 text-gray-700">
        <h1 className="text-4xl font-bold">Take the Questionnaire!</h1>
        <Formik
          onSubmit={async values => {
            let [V, A, R, K] = [0, 0, 0, 0];
            for (const key in values) {
              if (values[key]) {
                const [q, a] = key.split('_');
                const model = this.state.answers[q][a];

                // eslint-disable-next-line default-case
                switch (model) {
                  case 'V':
                    V++;
                    break;
                  case 'A':
                    A++;
                    break;
                  case 'R':
                    R++;
                    break;
                  case 'K':
                    K++;
                    break;
                }
              }
            }
            const style = this.getLearnerStyle({ V, A, R, K });
            if (style) {
              this.props.setLearnerStyle(style);

              if (this.props.user.email) {
                const {
                  firestore,
                  user: { email },
                } = this.props;

                try {
                  const u = await firestore.get({
                    collection: 'learner_styles',
                    where: ['user', '==', email],
                  });
                  if (u.docs.length) {
                    const { id } = u.docs[0];
                    firestore.update(
                      { collection: 'learner_styles', doc: id },
                      { style },
                    );
                  } else {
                    firestore.add(
                      { collection: 'learner_styles' },
                      { style, user: email },
                    );
                  }
                } catch (error) {
                  console.log('save', error);
                }
              }

              this.props.history.push('/dashboard');
            }
          }}
          render={props => (
            <Form>
              {this.state.questiones.map((q, index) => (
                <CheckBoxGroup key={q.question} qIndex={index} question={q} />
              ))}

              <button
                className="bg-black hover:bg-gray-700 text-white font-bold py-2 px-4 rounded-full "
                align="right"
                type="submit"
              >
                SUBMIT
              </button>
            </Form>
          )}
        />
      </div>
    );
  }
}

export default connect(
  ({ firebase }) => {
    return {
      user: firebase.profile,
    };
  },
  { setLearnerStyle },
)(withFirestore(withFirebase(Questionnaire)));
