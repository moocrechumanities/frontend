/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { searchAction } from '../../redux/actions';
import './search.css';
import { GridLoader } from 'react-spinners';

export class SearchPage extends Component {
  state = {
    searching: false,
    initState: true,
    noResult: false,
  };

  componentWillMount() {
    this.props.searchAction('');
  }

  search = e => {
    this.setState({ searching: true, initState: false, noResult: false });
    this.props.searchAction(e.target.value, noResult => {
      this.setState({ searching: false, noResult });
    });
  };

  getStyles = index => {
    if (index === 3) {
      return 'w-full shadow rounded p-4 mx-1 my-3 flex flex-wrap bg-gray-100 b-l-2';
    }

    if (index === 2 || index === 1) {
      return 'w-full shadow rounded p-4 mx-1 my-3 flex flex-wrap bg-gray-100 b-l-1';
    }

    if (index === 0) {
      return 'w-full shadow rounded p-4 mx-1 my-3 flex flex-wrap bg-gray-100 b-l';
    }
    return 'w-full shadow rounded p-4 mx-1 my-3 flex flex-wrap bg-gray-100';
  };

  getIcon = type => {
    if (type === 'video') {
      return <i className="fas fa-video" />;
    }
    if (type === 'reading') {
      return <i className="fas fa-file-pdf" />;
    }
  };

  renderType = type => {
    const icon = this.getIcon(type);
    return (
      <span>
        {icon} {type}
      </span>
    );
  };

  render() {
    return (
      <section className="container mx-auto">
        <div className="flex flex-wrap p-4">
          <div className="w-full">
            <div>
              <input
                className="w-full p-4 rounded-full border bg-gray-100"
                type="text"
                placeholder="Search"
                onKeyUp={this.search}
              />
            </div>
          </div>
          <div className="w-full mt-8 flex flex-wrap">
            {/* <div className="w-1/5">
              <div className="border rounded p-2 h-full">
                <div className="m-2">
                  <h3 className="font-bold">Lecturer Accent</h3>
                  <ul>
                    <li>
                      <input type="checkbox" />
                      {'   '} English - UK
                    </li>
                    <li>
                      {' '}
                      <input type="checkbox" /> {'   '}
                      English - US
                    </li>
                  </ul>
                </div>
              </div>
            </div> */}
            <div className="w-auto mx-auto px-4">
              {this.state.searching && (
                <div className="">
                  <GridLoader
                    sizeUnit="px"
                    size={30}
                    color="#285e61"
                    loading={this.state.searching}
                  />
                </div>
              )}
              {this.state.initState && (
                <div className="text-xl">
                  Type above in the field to begin search
                </div>
              )}
              {this.state.noResult && (
                <div>
                  <div className="mx-auto font-semibold text-center">
                    No Results Found!
                  </div>
                  <div>
                    <img src="https://cdn.dribbble.com/users/37530/screenshots/2485318/no-results.png" />
                  </div>
                </div>
              )}
              {this.props.searchResults.map(course => (
                <div
                  key={course.courseTitle}
                  className={this.getStyles(course.matchedIndex)}
                >
                  <div className="w-full mb-4 flex flex-wrap">
                    <div className="w-2/5">
                      <h3 className="text-teal-900 text font-bold">
                        {course.courseTitle}
                      </h3>
                    </div>
                    <div className="w-2/5  flex flex-wrap">
                      <div className="w-4/5">
                        <h3 className="text-teal-800">
                          {' '}
                          {`Pdfs: ${course.pdfWeight}%    Quizzes: ${
                            course.quizWeight
                          }%`}{' '}
                        </h3>
                      </div>
                    </div>
                    <div className="w-1/5 flex justify-end text-teal-900 font-bold">
                      <span>{course.data[0].courseSource}</span>
                    </div>
                  </div>
                  <div className="w-full">
                    {course.data.map(sub => (
                      <a href={sub.url} target="_blank" className="">
                        <div className="border rounded p-4 m-4 bg-gray-100">
                          <span className="px-2 py-1 mx-2 bg-teal-300 rounded text-teal-800">
                            {this.renderType(sub.type)}
                          </span>
                          <span className="text-teal-900">{sub.title}</span>
                        </div>
                      </a>
                    ))}
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default connect(
  ({ courses: { searchResults } }) => ({ searchResults }),
  { searchAction },
)(SearchPage);
