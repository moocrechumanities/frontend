/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import { connect } from 'react-redux';

export class DashboardPage extends Component {
  render() {
    const { learnerStyle } = this.props.user;
    return (
      <section className="m-4">
        <div className="flex flex-wrap justify-center">
          <div className="w-4/5">
            <div className="flex flex-wrap">
              {learnerStyle ? (
                <div className="w-full p-4">
                  <div>
                    <h1 className="text-lg">Recommended Courses</h1>
                  </div>
                  <div>
                    <div className=" flex flex-no-wrap py-2  overflow-x-auto overflow-y-hidden">
                      {this.props.courses.recommended.map(
                        ({ title, url, id, source }) => (
                          <a
                            target="_blank"
                            href={url}
                            key={id}
                            style={{ width: '200px', height: '300px' }}
                            className=" shadow-lg hover:shadow-none border rounded flex-shrink-0 mx-1 p-4 bg-gray-100"
                          >
                            <div className="flex flex-col justify-center h-full ">
                              <h3 className="text-center font-bold">{title}</h3>
                              <h3 className="text-center">by {source}</h3>
                            </div>
                          </a>
                        ),
                      )}
                    </div>
                  </div>
                </div>
              ) : (
                <div className="w-full p-4 flex flex-wrap items-center ">
                  <div className="mx-auto">
                    <img src="https://i.ibb.co/0ZB3gJt/2517915.jpg" />
                  </div>
                  <div className="mx-auto text-2xl font-semibold antialiased text-teal-700 tracking-wide">
                    You need to identify your learner style to get recommended
                    courses
                  </div>
                </div>
              )}

              {/* <div className="w-full p-4">
                <div>
                  <h1 className="text-lg">Recently searched courses</h1>
                </div>
                <div>
                  <div className=" flex flex-no-wrap py-2  overflow-x-auto overflow-y-hidden">
                    {this.props.courses.recentlySearched.map(
                      ({ title }, index) => (
                        <div
                          key={index}
                          style={{ width: '200px', height: '300px' }}
                          className="shadow-lg hover:shadow-none bg-white border rounded flex-shrink-0 mx-1 p-4"
                        >
                          <h3 className="text-center">{title}</h3>
                        </div>
                      ),
                    )}
                  </div>
                </div> */}
              {/* </div> */}
            </div>
          </div>
          {/* <div className="w-1/5 p-4">
            <div>
              <h1 className="text-lg invisible">Profile</h1>
            </div>
            <div className="p-4 my-2 bg-gray-100 border border-solid rounded flex flex-wrap">
              <div className="w-full">
                <img
                  className="m-auto w-40 rounded-full"
                  src="https://www.w3schools.com/howto/img_avatar.png"
                />
              </div>
              <div className="w-full my-2 text-center">
                <h4 className="text-lg">{this.props.user.name}</h4>
                <h3 className="font-bold">{this.props.user.learnerStyle}</h3>
              </div>
            </div>
          </div> */}
        </div>
      </section>
    );
  }
}

export default connect(({ user, courses }) => ({
  user,
  courses,
}))(DashboardPage);
