/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React, { Component } from 'react';
import { Switch, Route, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Tooltip from 'rc-tooltip';
import 'rc-tooltip/assets/bootstrap_white.css';
import { withFirebase, withFirestore } from 'react-redux-firebase';
import { AnimatedSwitch } from 'react-router-transition';
import Popover from '@terebentina/react-popover';
import '@terebentina/react-popover/lib/styles.css';
import HomePage from 'containers/HomePage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import Questionnaire from 'containers/Questionnaire/Loadable';

import GlobalStyle from '../../global-styles';
import DashboardPage from '../DashboardPage';
import SearchPage from '../SearchPage';

import { setLearnerStyle } from '../../redux/actions';

import './app.css';

export class App extends Component {
  componentDidMount() {
    this.props.setLearnerStyle(this.props.learnerStyle);
  }

  async componentDidUpdate(prevProps) {
    if (!this.props.learner_styles) {
      await this.props.firestore.get({ collection: 'learner_styles' });
    }
    if (this.props.profile.displayName !== prevProps.profile.displayName) {
      const { email } = this.props.profile;

      if (email) {
        const u = Object.values(this.props.learner_styles || []).find(
          l => l.user === email,
        );

        if (u) {
          console.log('>', u.style);
          this.props.setLearnerStyle(u.style);
        }
      }
    }
  }

  renderStyleTooltip = () => {
    const style = this.props.learnerStyle;
    let learnerStyle = '';

    if (style.includes('V')) {
      learnerStyle += 'Visual ';
    }

    if (style.includes('A')) {
      learnerStyle += 'Auditory ';
    }

    if (style.includes('R')) {
      learnerStyle += 'Reading ';
    }

    if (style.includes('K')) {
      learnerStyle += 'Kinesthetic ';
    }

    return <span>{learnerStyle}</span>;
  };

  loginWithGoogle = () => {
    this.props.firebase.login({ provider: 'google', type: 'popup' });
  };

  logout = () => {
    this.props.firebase.logout();
    this.props.setLearnerStyle('');
  };

  renderLoginButton = () => {
    if (this.props.profile.displayName) {
      const { displayName, avatarUrl } = this.props.profile;

      return (
        <Popover
          className="w-auto"
          position="bottom"
          trigger={
            <span className="">
              <img className="w-10 h-10 rounded-full ml-4" src={avatarUrl} />
            </span>
          }
        >
          <div style={{ width: '200px' }} className="flex flex-wrap">
            <div className="w-full">{displayName}</div>
            <div>
              <button onClick={this.logout}>Logout</button>
            </div>
          </div>
        </Popover>
      );
    }
    return (
      <button
        className="bg-transparent hover:bg-red-500 text-red-700 font-semibold hover:text-white py-2 px-4 border border-red-500 hover:border-transparent rounded"
        onClick={this.loginWithGoogle}
      >
        Sign In with Google
      </button>
    );
  };

  render() {
    const { learnerStyle } = this.props;

    return (
      <main>
        <div style={{ marginTop: '-50px' }} className="flex justify-between">
          <span>
            <a href="#">
              <img
                src="https://i.ibb.co/8jC1pjQ/8c30f0cd-6651-428c-b391-c122b2fda3f9-100x100.png"
                alt="8c30f0cd-6651-428c-b391-c122b2fda3f9-200x200"
                border="0"
              />
            </a>
          </span>
          <nav className="flex items-center justify-end mx-2 my-2 mt-4 content-center">
            <li className="list-none mx-3">
              <Link to="/">Home</Link>
            </li>
            <li className="list-none mx-3">
              <Link to="/search">Search</Link>
            </li>
            <li className="list-none mx-3">
              <Link to="/questionnaire">Questionnaire</Link>{' '}
            </li>
            <li className="list-none mx-3">
              <Link to="/recommended-courses">Recommended Courses</Link>
            </li>
            <li className="list-none mx-3">{this.renderLoginButton()}</li>
            <li className="list-none mx-3">
              {learnerStyle ? (
                <Tooltip placement="bottom" overlay={this.renderStyleTooltip()}>
                  <div className="bg-teal-400 py-2 px-4 rounded text-teal-900 font-semibold">
                    {learnerStyle && <span>Your Style - {learnerStyle}</span>}
                  </div>
                </Tooltip>
              ) : (
                <Link to="/questionnaire">
                  <div className="bg-teal-400 py-2 px-4 rounded text-teal-900 font-semibold">
                    Find Your Style
                  </div>
                </Link>
              )}
            </li>
          </nav>
        </div>
        <div>
          <div>
            <AnimatedSwitch
              atEnter={{ opacity: 0 }}
              atLeave={{ opacity: 0 }}
              atActive={{ opacity: 1 }}
              className="switch-wrapper"
            >
              <Route exact path="/" component={HomePage} />
              <Route exact path="/questionnaire" component={Questionnaire} />
              <Route path="/recommended-courses" component={DashboardPage} />
              <Route path="/search" component={SearchPage} />
              <Route component={NotFoundPage} />
            </AnimatedSwitch>
          </div>
          <GlobalStyle />
        </div>
      </main>
    );
  }
}

export default connect(
  ({
    user: { learnerStyle },
    firebase: { auth, profile },
    db: {
      data: { learner_styles },
    },
  }) => ({ learnerStyle, auth, profile, learner_styles }),
  { setLearnerStyle },
)(withFirestore(withFirebase(App)));
